const express = require('express');
const httpStatus = require('http-status');
const router = express.Router();
const pool = require('../controllers/connection')

//login /login?username=jvkj&&password=fjkdsj
router.get('/login', async function (req, res, next) {
    var username = req.query.username
    var password = req.query.password
    let query = 'SELECT userid FROM user WHERE username = ? AND password = ?'
    await pool.query(query, [username, password]).then(result => {
        if (!result.length){
            res.status(httpStatus.OK).json({userid: -1})
        } else {
            res.status(httpStatus.OK).json(result[0])
            console.log(result)
        }
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({userid: -1})
    })
})

//register /register send json
router.post('/register', async function (req, res, next) {
    var input = [req.query.username, req.query.password, req.query.firstname, req.query.lastname, req.query.phone, req.query.email, req.query.usertype]
    let query = 'INSERT INTO user (username, password, firstname, lastname, phone, email, usertype) VALUES (?)'
    await pool.query(query, [input]).then(result => {
        res.status(httpStatus.OK).json({message: true})
    }).catch(err => {
        console.log(err)
        if (err.code === 'ER_DUP_ENTRY'){
            res.status(httpStatus.BAD_REQUEST).json({message: false})
        }
        res.status(httpStatus.BAD_REQUEST).json({message: false})
    })
})

//listLand /listland?userid=jfjdk
router.get('/listland', async function (req, res, next) {
    var userid = req.query.userid
    let query = 'SELECT * FROM onsale_land WHERE userid = ? '
    await pool.query(query, [userid]).then(result => {
        if (!result.length){
            res.status(httpStatus.OK).json({result:  null})
        } else {
            var w = []
            var isOnSale = false
            for (i = 0; i < result.length; i++){
                if (result[i].onsaleid == null){
                    isOnSale = false
                }else {
                    isOnSale = true
                }
                w.push({deedid: result[i].deedid, titleNo: result[i].titleNo, isOnSale: isOnSale})
            }
            res.status(httpStatus.OK).json({result: w})
        }
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: null})
    })
})

//landprofile
router.get('/landprofile', async function (req, res, next) {
    var deedid = req.query.deedid
    let query = 'SELECT  * FROM deed_map WHERE deedid = ?'

    await pool.query(query, [deedid]).then(result => {
        res.status(httpStatus.OK).json(result[0])
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: null})
    })
})

// add land
router.post('/addland', async function (req, res, next) {
    try {
        let querydeed = 'INSERT INTO deed (titleNo, zone, userid, street, subdistrict, district, province, postalcode) VALUES (?)'
        var deedy = [req.query.titleNo, req.query.zone, req.query.userid, req.query.street, req.query.subdistrict, req.query.district, req.query.province, req.query.postalcode]
        console.log(deedy)
        await pool.query(querydeed, [deedy]).then(result => {
            req.deedtitle = req.query.titleNo
            next()
        }).catch(err => {
            console.log(err)
            res.status(httpStatus.BAD_REQUEST).json({result: false})
        })
    } catch(err) {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    }
})

router.post('/addland', async function (req, res, next) {
    let searchdeed = 'SELECT deedid FROM deed WHERE titleNo = ?'
    var deedtitle = req.deedtitle

    await pool.query(searchdeed, [deedtitle]).then(result => {
        req.deedid = result[0].deedid 
        next()
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result : false})
    })
})

router.post('/addland', async function (req, res, next){
    let querymap = 'INSERT INTO map (deedid, latitude, longtitude) VALUES (?)'
    var deedid = req.deedid
    var mappy = [deedid, req.query.latitude, req.query.longtitude]

    await pool.query(querymap, [mappy]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })

})

router.get('/deleteland', async function (req, res, next){
    let query = 'DELETE FROM deed WHERE deedid = ?'
    var deedid = req.query.deedid

    await pool.query(query, [deedid]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })
})

router.post('/updateland', async function (req, res, next){
    var deedid = req.query.deedid
    var updatedeed = 'UPDATE deed SET titleNo = ?, street = ?, subdistrict = ?, district = ?, province = ?, postalcode = ?, zone = ? WHERE deedid = ?'

    await pool.query(updatedeed, [req.query.titleNo, req.query.street, req.query.subdistrict, req.query.district, req.query.province, req.query.postalcode, req.query.zone, deedid]).then(result => {
        next()
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })

})

router.post('/updateland', async function (req, res, next){
    var deedid = req.query.deedid
    var updatemap = 'UPDATE map SET latitude = ? , longtitude = ? WHERE deedid = ?'

    await pool.query(updatemap, [req.query.latitude, req.query.longtitude, deedid]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })

})

router.get('/listfiltermarket', async function (req, res ,next){
    var priceSelect = req.query.priceSelect

    var selectLandQuery = 'SELECT * FROM marketlist WHERE pricesale BETWEEN ? AND ?'
    
    var pricerange1 = 0
    var pricerange2 = 0

    if (priceSelect == 'A'){
        pricerange1 = 1000000
        pricerange2 = 3000000
    }else if (priceSelect == 'B'){
        pricerange1 = 3000000
        pricerange2 = 5000000
    }else if (priceSelect == 'C'){
        pricerange1 = 5000000
        pricerange2 = 7000000
    }

    await pool.query(selectLandQuery, [pricerange1, pricerange2]).then(result =>{
        res.status(httpStatus.OK).json({result: result})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: null})
    })

})

router.get('/listallmarket', async function (req, res, next){
    var query = 'SELECT * FROM marketlist'

    await pool.query(query).then(result =>{
        res.status(httpStatus.OK).json({result: result})
    }).catch(err =>{
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: null})
    })

})

router.post('/enableOnSale', async function (req, res, next){
    var deedid = req.query.deedid
    var pricesale = req.query.pricesale
    var datesale = new Date();

    var value = [deedid, pricesale, datesale]

    var query = 'INSERT INTO onsale (deedid, pricesale, datesale) VALUES (?)'

    await pool.query(query, [value]).then(result =>{
        res.status(httpStatus.OK).json({result: true})
    }).catch(err =>{
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })
})

router.get('/disableOnsale', async function (req, res, next){
    var deedid = req.query.deedid
    var query = 'DELETE from onsale WHERE deedid = ?'

    await pool.query(query, [deedid]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })

})

router.post('/payment', async function (req, res, next){
    var paymentdate = new Date();
    var value = [req.query.amount, req.query.userid, req.query.deedid, paymentdate]

    var query = 'INSERT INTO payment (amount, userid, deedid, paymentdate) VALUES (?)'

    await pool.query(query, [value]).then(result => {
        next()
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })
})

router.post('/payment', async function (req, res, next){
    var query = 'UPDATE deed SET userid = ? WHERE deedid = ?'
    var userid = req.query.userid
    var deedid = req.query.deedid

    await pool.query(query, [userid, deedid]).then(result => {
        next()
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })
})

router.post('/payment', async function (req, res, next){
    var query = 'DELETE FROM onsale WHERE deedid = ?'
    var deedid = req.query.deedid

    await pool.query(query, [deedid]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err => {
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result : false})
    })
})

//deelte from onsale

router.post('/countLandview', async function (req, res, next){
    var query = 'INSERT INTO landview (onsaleid, userid, timestamp) VALUES (?)'
    var time = new Date();

    var value = [req.query.onsaleid, req.query.userid, time]

    await pool.query(query, [value]).then(result => {
        res.status(httpStatus.OK).json({result: true})
    }).catch(err =>{
        console.log(err)
        res.status(httpStatus.BAD_REQUEST).json({result: false})
    })
})

module.exports.router = router;