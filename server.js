const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const httpStatus = require('http-status');

const app = express();

var port = process.env.PORT || 3000;

//Bodyparser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//Link to routers
const page = require('./routes/page').router;
app.use('/', page);


//port
app.listen(port, function(){
    console.log('running');
})
