const mysql = require('mysql');
const util = require('util')

//Connecting to db
var config =
{
    connectionLimit: 1000,
    host: 'meelando.cmicaizbrzza.ap-southeast-1.rds.amazonaws.com',
    user: 'meowmoee',
    password : 'Tansuwan18',
    database: 'meeland',
    port: 3306
};

const pool = mysql.createPool(config);

// Ping database to check for common exception errors.
pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }

    if (connection) connection.release()
    return
})

// Promisify for Node.js async/await.
pool.query = util.promisify(pool.query)

module.exports = pool